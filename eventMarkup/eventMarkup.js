// $Id$
/** @jsx zdom */
/* global zmail, zmUtil, zmText, zmContentCache, zmConUtil, zmPreviewTemplate,zmResource,zmAjq */

import {constructTemplates} from "./eventMarkupTemplates.js";
import {I18N, links} from "./constants.js";


var mailDataObj, calObj, msgId,
    attendeeCount, currentStatus, toMailId, evtType, Info, attendeeList, timeObj;
var _previewText = zmText.get("preview"); // NO I18N
// utils

var getAttendeeMailId = function (toList) {
    var finalId = "",
        mailIdList = [];
    finalId = toList[0].eid;
    if (toList.length > 1) {
        for (var i in zmail.aInfo) {
            mailIdList.push(zmail.aInfo[i].mailId.toLowerCase());
        }

        // Add alias details from send mail details
        if (zmail.cmpdata && zmail.cmpdata.FROM_LIST) {
            var sendMailDetails = zmail.cmpdata.FROM_LIST;
            for (var id in sendMailDetails) {
                if (mailIdList.indexOf(id) === -1) {
                    mailIdList.push(id.toLowerCase());
                }
            }
        }

        for (var j = 0; j < toList.length; j++) {
            if (mailIdList.indexOf(toList[j].eid.toLowerCase()) !== -1) {
                finalId = toList[j].eid;
                break;
            }
        }
    }
    return finalId;
};

var getPhotoSrc = function () {

    var contactId,
        imgsrc = zmResource.get("image", "SC-Photo.png"); // NO I18N

    contactId = mailDataObj.FROM;

    var contact = null;

    contact = zmail.ContactBook.get({
        promise: false,
        eid: contactId,
        type: [ "personal", "org" ] // NO I18N
    })[0];

    if (contact) {
        imgsrc = contact.photo(false);
    }

    return imgsrc;
};

var parseDate = function (time) {
    var dateObj = {};
    if (time) {
        var splitArr = time.split(","); // NO I18N
        if (splitArr.length > 0) {
            var splitDate = splitArr[0];
            if (splitDate) {
                var splitDateArr = splitDate.split(" "); // NO I18N
                if (splitDateArr.length === 3) {
                    dateObj.day = splitDateArr[0];
                    dateObj.month = splitDateArr[1];
                    dateObj.date = splitDateArr[2];
                }
            }
        }
    }
    return dateObj;
};

// Templates
var _templates = {
    eventCreatedAndAdded: function (evtStatus) {
        var config = {
            displayInviteCount: true,
            displayAttendeeList: true,
            displayInviteStatus: true,
            displayInviteMsg: true,
            links: [
                "viewEvent"
            ],
            inviteStatus: evtStatus
        };
        return constructTemplates( {config, mailDataObj, calObj, timeObj });
    },
    eventCreatedAndNotAdded: function (evtStatus) {
        var config = {
            displayInviteCount: true,
            displayAttendeeList: true,
            displayInviteMsg: true,
            links: [
                "addToCalender"
            ],
            inviteStatus: evtStatus
        };
        return constructTemplates( {config, mailDataObj, calObj, timeObj });
    },

    inviteAccepted: function () {
        var config = {
            event: "eventAccepted",
            responseMessage: I18N.acceptedmsg
        };
        return constructTemplates( {config, mailDataObj, calObj, timeObj });
    },

    inviteDeclined: function () {
        var config = {
            event: "eventDeclined",
            responseMessage: I18N.declinedmsg
        };
        return constructTemplates( {config, mailDataObj, calObj, timeObj });
    },

    inviteTentativelyAccepted: function () {
        var config = {
            event: "eventMaybe",
            responseMessage: I18N.tentativemsg
        };
        return constructTemplates( {config, mailDataObj, calObj, timeObj });
    },

    eventCancelled: function () {
        var config = {
            event: "eventCancelled",
            stamp: "cancelled"
        };
        return constructTemplates( {config, mailDataObj, calObj, timeObj });
    },

    eventAddedExpired: function (evtStatus) {
        var config = {
            event: "eventExpired",
            displayInviteCount: true,
            displayAttendeeList: true,
            displayInviteStatus: true,
            displayInviteMsg: true,
            stamp: "expired",
            inviteStatus: evtStatus,
            links: [
                "deleteEvent",
                "viewEvent"
            ]
        };
        return constructTemplates( {config, mailDataObj, calObj, timeObj });
    },

    eventNotAddedExpired: function () {
        var config = {
            event: "eventExpired",
            displayInviteCount: true,
            displayAttendeeList: true,
            displayInviteMsg: true,
            stamp: "expired"
        };
        return constructTemplates( {config, mailDataObj, calObj, timeObj });
    },

    eventInfo: function () {
        var config = {
            displayInviteCount: true,
            displayAttendeeList: true,
            displayInviteMsg: true
        };
        return constructTemplates( {config, mailDataObj, calObj, timeObj });
    },

    eventsNotImported: function () {
        var config = {
            displayInviteCount: true,
            displayAttendeeList: true,
            links: [
                "addtocalendar"
            ],
            displayInviteMsg: true
        };
        return constructTemplates( {config, mailDataObj, calObj, timeObj });
    }
};

// action methods
var _actions = {
    getAttendeeStatus: function () {
        var deferred = $.Deferred();
        var callbk = function (response) {
            deferred.resolve();
            return response;
        };
        var errorCallbk = function (response) {
            deferred.resolve();
            return response;
        };

        var params = {
            "toMailId": toMailId, // NO I18N
            "jsoparam": JSON.stringify(calObj),	// NO I18N
            mstpcrdhkur: zmAjq.getCookie("zmcsr")// NO I18N
        };

        // NOTE : calendar response can be json/string. So dataType:"json" is vomitted here  // NO I18N
        var xhrObj = {
            url: zmail.conPath + "/zcal/api/private/json/getAttendeeStatus", // NO I18N
            type: "POST", // NO I18N
            success: callbk,
            error: errorCallbk,
            data: params
        };
        return $.ajax(xhrObj);

    },

    showEventStatus: function (elemRef, prefix) {
        var eventDOM;
        switch (currentStatus) {
            case "nostatus": // NO I18N
            case "yes": // NO I18N
            case "no": // NO I18N
            case "maybe": // NO I18N
                eventDOM = _templates.eventCreatedAndAdded(currentStatus);
                break;
            case "disable": // NO I18N
                eventDOM = _templates.eventNotAddedExpired();
                break;
            case "disable:0": // NO I18N
                eventDOM = _templates.eventAddedExpired("nostatus"); // NO I18N
                break;
            case "disable:1": // NO I18N
                eventDOM = _templates.eventAddedExpired("yes"); // NO I18N
                break;
            case "disable:2": // NO I18N
                eventDOM = _templates.eventAddedExpired("no"); // NO I18N
                break;
            case "disable:3": // NO I18N
                eventDOM = _templates.eventAddedExpired("maybe"); // NO I18N
                break;
            case "No Change": // NO I18N
                if (evtType === "CANCEL") { // NO I18N
                    eventDOM = _templates.eventCancelled();
                } else if (evtType === "REPLY") { // NO I18N
                    if (calObj.status === "accepted") { // NO I18N
                        eventDOM = _templates.inviteAccepted();
                    } else if (calObj.status === "declined") { // NO I18N
                        eventDOM = _templates.inviteDeclined();
                    } else if (calObj.status === "tentatively accepted") { // NO I18N
                        eventDOM = _templates.inviteTentativelyAccepted();
                    }
                } else if (evtType === "ADD" || evtType === "COUNTER" || evtType === "REFRESH" || // NO I18N
                    evtType === "DECLINECOUNTER") {	// NO I18N
                    eventDOM = _templates.eventInfo();
                }
                break;
            case "Add Event": // NO I18N
                eventDOM = _templates.eventsNotImported();
                break;
            case "Update Event": // NO I18N
                eventDOM = _templates.eventInfo();
                break;
            default:
                eventDOM = _templates.eventCreatedAndNotAdded();
                break;
        }
        prefix = prefix || Info.prefix;
        var contentElem = (Info && Info.from === "print") ? $.e(elemRef) : // NO I18N
            $.e(elemRef).find("#zmCon" + prefix + msgId); // NO I18N
        if (!contentElem.find("div.zmEventInfo").length) { // NO I18N
            contentElem.find("div.zmPCnt").prependDOM(eventDOM); // NO I18N
        }
        return eventDOM;
    },

    parseStatusResponse: function (data, gId) {
        var finalStr = "";

        if (evtType === "REQUEST") { // NO I18N
            data = $.parseJSON(data, gId);
            // Reset invitaion time based on user timezone
            if (data.time) {
                calObj.time = data.time;
            }
            // Check if event added
            if (data.isEventAvailable !== undefined && data.isEventAvailable) {
                if (data.isExpired !== undefined && data.isExpired) {
                    if (data.status !== undefined) {
                        return "disable:" + data.status; // NO I18N
                    } else if (calObj.service === "zcrm") {// NO I18N
                        /* CRM event is stored in org calendar. On delete event, only the invitee
                            is removed from the invitation but event exists in CRM */
                        return "disable"; // NO I18N
                    }
                } else if (data.status !== undefined) {
                    data = String(data.status);
                }
            } else if (data.isExpired !== undefined && data.isExpired) {
                return "disable"; // NO I18N
            } else if (gId && attendeeList.indexOf(zmail.aInfo[zmail.accId].mailId) === -1) {
                return "Update Event"; // NO I18N
            }
        } else if (evtType === "CANCEL" || evtType === "PUBLISH") { // NO I18N
            return data;
        }

        if (data.length <= 3) {
            switch (data) {
                case "0": // NO I18N
                    finalStr = "nostatus"; // NO I18N
                    break;
                case "1": // NO I18N
                    finalStr = "yes"; // NO I18N
                    break;
                case "2": // NO I18N
                    finalStr = "no"; // NO I18N
                    break;
                case "3": // NO I18N
                    finalStr = "maybe"; // NO I18N
                    break;
                case "4": // NO I18N
                    finalStr = "disable"; // NO I18N
                    break;
            }
        }

        return finalStr;

    },

    updateStatus: function (sts, elem) {
        if (sts) {

            var params = {
                "jsoparam": JSON.stringify(calObj), // NO I18N
                "toMailId": toMailId, // NO I18N
                "status": sts, // NO I18N
                mstpcrdhkur: zmAjq.getCookie("zmcsr")// NO I18N
            };

            var addSelection = function () {
                if (elem) {
                    $.e(elem).closest(".zmEventMailBtnWra").children().removeClass("sel"); // NO I18N
                    $.e(elem).addClass("sel"); // NO I18N
                }
            };

            var callbk = function (resp) {
                if (!(resp instanceof Object)) {
                    resp = resp.trim();
                    if (resp === "Status Updated") { // NO I18N
                        currentStatus = sts;
                        zmUtil.succErrMsg('s', I18N.statusupdated); // NO I18N
                        addSelection();
                        zmPreviewTemplate.updateRSVPSchema(msgId, sts);
                    } else if (resp === "Status Update Failed") { // NO I18N
                        zmUtil.succErrMsg('e', I18N.statusfailed); // NO I18N
                    } else if (resp === "Event Expired") { // NO I18N
                        zmUtil.succErrMsg('e', I18N.expired); // NO I18N
                    } else if (resp === "Event Not present in calendar") { // NO I18N
                        zmUtil.succErrMsg('e', I18N.noevent); // NO I18N
                    }
                }
            };

            var errorCallbk = function (resp) {
                if (resp) {
                    if (resp instanceof Object) {
                        zmUtil.succErrMsg('e', resp.statusText); // NO I18N
                    } else {
                        zmUtil.succErrMsg('e', I18N.statusfailed); // NO I18N
                    }
                }
            };

            // NOTE : calendar response can be json/string. So dataType:"json" is vomitted here  // NO I18N
            var xhrObj = {
                url: zmail.conPath + "/zcal/api/private/json/StatusUpdate", // NO I18N
                type: "POST", // NO I18N
                success: callbk,
                error: errorCallbk,
                data: params
            };

            $.ajax(xhrObj);
        }
    },

    acceptInvitation: function (elem) {
        if (currentStatus !== "yes") { // NO I18N
            _actions.updateStatus("yes", elem); // NO I18N
        }
    },

    declineInvitation: function (elem) {
        if (currentStatus !== "no") { // NO I18N
            _actions.updateStatus("no", elem); // NO I18N
        }
    },

    tentativeAcceptance: function (elem) {
        if (currentStatus !== "maybe") { // NO I18N
            _actions.updateStatus("maybe", elem); // NO I18N
        }
    },

    viewEventInCalendar: function () {
        var calObjWODesc = $.extend({}, calObj); // calendar object without description.
        delete calObjWODesc.desc;
        var url = "//" + zmail.urls.calURL + "/mailapiact.do?mode=viewmaileve&jsoparam=" + // NO I18N
            encodeURIComponent(JSON.stringify(calObjWODesc));
        var caltab = window.open(url, '_blank'); // NO I18N
        caltab.focus();
    },

    addEventToCalendar: function (elem) {

        var removeExtraLink = function () {
            if (elem) {
                $.e(elem).removeClass('jsAddToCal').addClass('jsviewEvent');
            }
        };

        var showViewEventLink = function () {
            if (elem) {
                $.e(elem).text(links.viewEvent.I18N);
            }
        };
        var callbk = function (resp) {
            if (resp) {
                try {
                    $.parseJSON(resp);
                    resp = $.parseJSON(resp);
                } catch (e) { }
                if (resp instanceof Object) {
                    /* Sample error response
                    {response:{uri:”/mailapiact.do?mode=addmaileve“,
                        error : {“code”: “1801”, "message","Error occured while processing this request”}}}
                    */
                    resp = resp.response ? resp.response : resp;
                    if (resp.error && resp.error.message) {
                        zmUtil.succErrMsg('e', resp.error.message);// NO I18N
                    } else {
                        zmUtil.succErrMsg('e', _previewText.attachmentmenu.eventerror);// NO I18N
                    }

                } else {
                    resp = resp.trim();
                    if (resp === "Added Successfully") { // NO I18N
                        zmUtil.succErrMsg('s', _previewText.attachmentmenu.eventadded); // NO I18N
                        showViewEventLink();
                        removeExtraLink();
                    } else if (resp === "Event Updated Successfully") { // NO I18N
                        zmUtil.succErrMsg('s', _previewText.attachmentmenu.eventupdated); // NO I18N
                        showViewEventLink();
                        removeExtraLink();
                    }
                }
            }
        };

        var errorCallbk = function (resp) {
            if (resp) {
                if (resp instanceof Object) {
                    zmUtil.succErrMsg('e', resp.statusText); // NO I18N
                } else {
                    zmUtil.succErrMsg('e', _previewText.attachmentmenu.eventerror); // NO I18N
                }
            }
        };

        var xhrObj = {
            url: zmail.conPath + "/zcal/mailapiact.do?mode=addmaileve", // NO I18N
            type: "POST", // NO I18N
            success: callbk,
            error: errorCallbk,
            data: { msgId: msgId, accId: zmail.accId, jsoparam: JSON.stringify(calObj),
                mstpcrdhkur: zmAjq.getCookie("zmcsr")// NO I18N
            }
        };
        $.ajax(xhrObj);

    },

    deleteEventFromCalendar: function (elem) {

        var params = {
            "jsoparam": JSON.stringify(calObj), // NO I18N
            mstpcrdhkur: zmAjq.getCookie("zmcsr")// NO I18N
        };

        var removeExtraLink = function () {
            if (elem) {
                $.q('.zmEventMailALinks').remove();
                $.q('.jsviewEvent').remove();
                $.q('.jsDeleteEvent').remove();
            }
        };

        var callbk = function (resp) {
            var reqStatus;
            if (resp.response !== undefined && resp.response.result) {
                reqStatus = resp.response.result.eKey;
                removeExtraLink();
                if (reqStatus !== "0") { // NO I18N
                    zmUtil.succErrMsg('s', I18N.eventdeleted); // NO I18N
                } else if (reqStatus === "0") { // NO I18N
                    zmUtil.succErrMsg('s', I18N.deletedalready); // NO I18N
                }
            }
        };

        var errorCallbk = function (resp) {
            if (resp) {
                if (resp instanceof Object) {
                    zmUtil.succErrMsg('e', resp.statusText); // NO I18N
                } else {
                    zmUtil.succErrMsg('e', I18N.deletionfailed); // NO I18N
                }
            }
        };

        // NOTE : calendar response can be json/string. So dataType:"json" is vomitted here  // NO I18N

        var xhrObj = {
            url: zmail.conPath + "/zcal/mailapiact.do?mode=deletemaileve", // NO I18N
            type: "POST", // NO I18N
            success: callbk,
            error: errorCallbk,
            data: params
        };

        $.ajax(xhrObj);

    },

    bindEvents: function (eventDOM) {
        var container = $.e(eventDOM);
        var selectors = [
            '.zmEventMailBtnWra',
            '.zmEventMailALinks'
        ].join();
        container.on('click', selectors, function (eve) { // NO I18N
            var elem = eve.srcElement || eve.target;
            if ($.e(elem).hasClass("greenBtn")) {
                $.e(elem).closest(".zmEventMailBtnWra").children().removeClass("sel"); // NO I18N
                $.e(elem).addClass("sel"); // NO I18N
                _actions.acceptInvitation(elem);
            } else if ($.e(elem).hasClass("redBtn")) {
                $.e(elem).closest(".zmEventMailBtnWra").children().removeClass("sel"); // NO I18N
                $.e(elem).addClass("sel"); // NO I18N
                _actions.declineInvitation(elem);
            } else if ($.e(elem).hasClass("blueBtn")) {
                $.e(elem).closest(".zmEventMailBtnwra").children().removeClass("sel"); // NO I18N
                $.e(elem).addClass("sel"); // NO I18N
                _actions.tentativeAcceptance(elem);
            } else if ($.e(elem).hasClass(links.addToCalender.jsClass)) {
                _actions.addEventToCalendar(elem);
            } else if ($.e(elem).hasClass(links.viewEvent.jsClass)) {
                _actions.viewEventInCalendar(elem);
            } else if ($.e(elem).hasClass(links.deleteEvent.jsClass)) {
                _actions.deleteEventFromCalendar(elem);
            }
        });
    }
};

var construct = function (mId, elem, details) {
    if (!mId) {
        return;
    }
    var groupId,
        prefix = details && details.prefix,
        indx, eventRef;
    Info = details || {};
    msgId = mId;
    mailDataObj = zmContentCache.get(msgId);
    calObj = mailDataObj.CALOBJ;
    groupId = calObj.GroupId;
    timeObj = {
        eventDateInfo: parseDate(calObj.time),
        contactImgSrc: getPhotoSrc()
    };
    attendeeCount = calObj.attendee ? calObj.attendee.length : 0;
    currentStatus = "";
    toMailId = "";
    evtType = calObj.mtd;
    attendeeList = [];
    toMailId = mailDataObj.TO || "";
    toMailId = zmConUtil.splitncheck(toMailId, "object");	// NO I18N
    if (calObj.desc) {
        calObj.desc = calObj.desc.replace(/\\n/g, "\n").replace(/\\t/g, "\t")
            .replace(/\\r/g, "\r").replace(/\\b/g, "\b").replace(/\\v/g, "\v").replace(/\\f/g, "\f"); // NO I18N
    }
    if (toMailId.length) {
        toMailId = getAttendeeMailId(toMailId);
    } else {
        toMailId = "";
    }
    for (indx = 0; indx < attendeeCount; indx++) {
        attendeeList.push(calObj.attendee[indx].emailId);
    }
    if (evtType === "PUBLISH" || evtType === "REQUEST" || evtType === "CANCEL") { // NO I18N
        /* NOTE: "PUBLISH" type is handled for inline invitation */ // NO I18N
        $.when(_actions.getAttendeeStatus()).then(function (resp) {
            currentStatus = _actions.parseStatusResponse(resp, groupId);
            currentStatus = currentStatus.trim();
            eventRef = _actions.showEventStatus(elem, prefix);
            if (Info && Info.from !== "print") { // NO I18N
                _actions.bindEvents(eventRef);
            }
            zmPreviewTemplate.updateRSVPSchema(msgId, currentStatus);
        });
    } else if (evtType === "REPLY" || evtType === "COUNTER" || evtType === "ADD" || // NO I18N
        evtType === "REFRESH" || evtType === "DECLINECOUNTER") { // NO I18N
        currentStatus = "No Change"; // NO I18N
        eventRef = _actions.showEventStatus(elem);
        if (Info && Info.from !== "print") { // NO I18N
            _actions.bindEvents(eventRef);
        }
    }
};

var module = zmail.Core.Namespaces.create("zmEventMarkup"); // NO I18N
module.construct = construct;