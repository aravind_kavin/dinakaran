// $Id:$
/* global zmText */

let I18N = zmText.get("preview").calendar; // NO I18N

let classes = {
    disable: "zmDisabled",
    eventExpired: "eventExpired",
    themeColorForText: "zmThmClr"
};

let links = {
    addToCalender: {
        I18N: I18N.addtocal,
        jsClass: 'jsAddToCal'
    },
    viewEvent: {
        I18N: I18N.viewevent,
        jsClass: 'jsviewEvent'
    },
    deleteEvent: {
        I18N: I18N.deleteevent,
        jsClass: 'jsDeleteEvent'
    }
};

const eventStatus = {
    yes: {
        I18N: I18N.yes,
        class: 'greenBtn'
    },
    no: {
        I18N: I18N.no,
        class: 'redBtn'
    },
    maybe: {
        I18N: I18N.maybe,
        class: 'blueBtn'
    }
};

const stamps = {
    expired: I18N.expired,
    cancelled: I18N.cancelled
};


export {classes, stamps, eventStatus, links, I18N};