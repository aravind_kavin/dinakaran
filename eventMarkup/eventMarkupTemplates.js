// $Id:$
/** @jsx zdom */
/* global zmText */
import {classes, eventStatus, stamps, links, I18N} from "./constants.js";

let constructTemplates = ({config, mailDataObj, calObj, timeObj}) => {

    var eventDateInfo, contactImgSrc, attendeeCount, outerClass, isExpired;

    var eventAttendees = function () {
        var attendeeObj = [];
        attendeeObj = calObj.attendee.map(({emailId}) => {
            return emailId;
        }).join(", ");

        var attendeeTemplate = <span className={classes.themeColorForText}>  {attendeeObj}   </span>;
        return attendeeTemplate;
    };

    var displayStatusButtons = function () {
        return (
            <fragment>
                {
                    Object.keys(eventStatus).map((key) => {
                        return (<div className={"zmEventMailBtn " + eventStatus[key].class + (
                            (config.inviteStatus === key) ?
                                ' sel' : ' ')
                        } key={key.I18N}>
                            {eventStatus[key].I18N}
                        </div> );
                    })
                }
            </fragment>
        );
    };

    var displayActionLinks = function () {
        return (
            <fragment>
                {
                    (config.links).map((item, index) => {
                        return (
                            <fragment key={links[item].I18N}>
                                {(index > 0 ? <span className="zmSDot" /> : '')}
                                <span className={"zmLink " + links[item].jsClass}>
                                    {links[item].I18N}</span>
                            </fragment>
                        );
                    })
                }
            </fragment>
        );
    };

    eventDateInfo = timeObj.eventDateInfo;
    contactImgSrc = timeObj.contactImgSrc;
    attendeeCount = calObj.attendee ? calObj.attendee.length : 0;
    outerClass = config.event || "";
    isExpired = (outerClass === (classes.eventExpired));

    var markupTemplate = (
        <div className={"zmEventMail " + outerClass} >
            { (config.responseMessage) ?
                (
                    <div className="zmEventMailStatus">
                        {zmText.applyArgs(config.responseMessage,
                            { "sendername": mailDataObj.SENDER })}
                    </div>
                ) : ' '
            }

            <div className="zmEventMailWra">

                <div className={"zmEventMailLft " + (isExpired ? classes.disable : '')}>

                    <div className="zmEventMailDate">
                        <span>{eventDateInfo.month}</span>
                        <strong className={classes.themeColorForText}>{eventDateInfo.date}</strong>
                        <span>{eventDateInfo.day}</span>
                    </div>

                    { (config.displayInviteCount) ?
                        (
                            <div className="zmEventMailIC">
                                <strong>{attendeeCount}</strong>
                                <span> {I18N.invited}</span>
                            </div>
                        ) : ' '
                    }
                </div>

                <div className="zmEventMailRgt">

                    <div className={"zmEventMailOr " + (isExpired ? classes.disable : '')}>

                        <div className="SC_avtr">
                            <img src={contactImgSrc} />
                        </div>

                        <div className={"zmEventMailOrInfo " + (isExpired ?
                            classes.disable : '')}>

                            <strong>{mailDataObj.SENDER}</strong>
                            <span className="zmThmClr">
                                {mailDataObj.FROM}
                            </span>
                            { (config.displayInviteMsg) ?
                                (<span>{I18N.invitesyou}</span>) : ' '}
                        </div>
                    </div>

                    <div className={"zmEventMailInfo " + (isExpired ?
                        classes.disable : '')}>
                        <h3>{calObj.title}</h3>
                        <div>
                            <i className="msi-clock" />
                            <span>  {calObj.time} </span>
                        </div>

                        {
                            (calObj.loc !== "") ? (
                                <div>
                                    <i className="msi-location" />
                                    <span>{calObj.loc}</span>
                                </div>
                            ) : ''
                        }
                        {
                            (config.displayAttendeeList) ? (
                                <div><i className="msi-group" />
                                    {eventAttendees()}
                                </div>
                            ) : ''
                        }
                    </div>

                    { (config.links) ? (
                        <div className="zmEventMailAction">
                            {
                                (config.displayInviteStatus) ? (
                                    <div className={"zmEventMailBtnWra " + (isExpired ? classes.disable : '')}>
                                        {displayStatusButtons()}
                                    </div>
                                ) : ''
                            }
                            {
                                (config.links) ? (
                                    <div className="zmEventMailALinks">
                                        { (config.links) ? (displayActionLinks()) : ''}
                                    </div>
                                ) : ''
                            }
                        </div>
                    ) : ''
                    }
                </div>
            </div>
            {
                (config.stamp) ?
                    (
                        <div className="zmEventMailTag">
                            <span>{stamps[config.stamp]}</span>
                        </div>
                    ) : ''
            }
        </div>
    );

    return markupTemplate;
};

export {constructTemplates};