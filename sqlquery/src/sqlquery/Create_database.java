package sqlquery;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.File;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Create_database
 */
@WebServlet("/Create_database")
public class Create_database extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Create_database() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("text/html");
       
        String location="E:\\"+request.getParameter("dname");
        
        File file=new File(location);
        if (!file.exists()) {
            if (file.mkdir()) {
          
            //	printWriter.println("Database is created!");
            	String Message="Database created Successfully";
            	request.setAttribute("message", Message);
            	request.getRequestDispatcher("sqlquery.jsp").forward(request, response);
            	
            } else {
            	String Message="Failed to Create Database";
            	request.setAttribute("message", Message);
            	request.getRequestDispatcher("sqlquery.jsp").forward(request, response);
            	
            }
        }
        else
        {
        	String Message="Database Already Present";
        	request.setAttribute("message", Message);
        	request.getRequestDispatcher("sqlquery.jsp").forward(request, response);
        }
		//response.getWriter().append("Served at: ").append(request.getContextPath());
      //  request.getRequestDispatcher("sqlquery.jsp").forward(request, response);
      //  response.sendRedirect("sqlquery.jsp?message=success");
    //    request.getRequestDispatcher("sqlquery.jsp").forward(request, response);
		doGet(request, response);
	}

}
